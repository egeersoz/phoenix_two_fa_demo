# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :two_fa_demo,
  ecto_repos: [TwoFaDemo.Repo]

# Configures the endpoint
config :two_fa_demo, TwoFaDemoWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "teESKjReAQBDqXp+8y0XuKVQQXWfAzlu8uy6QJe3z8XMxcVv/GHL8akE3xC3YqAi",
  render_errors: [view: TwoFaDemoWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: TwoFaDemo.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
