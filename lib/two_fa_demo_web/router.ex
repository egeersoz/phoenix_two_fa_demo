defmodule TwoFaDemoWeb.Router do
  use TwoFaDemoWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :protected do
    plug TwoFaDemoWeb.Plugs.EnsureAuthenticated
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", TwoFaDemoWeb do
    pipe_through :browser

    get "/", PageController, :index
    resources "/users", UserController, except: [:index]
    resources "/sessions", SessionController, except: [:delete]
  end

  scope "/", TwoFaDemoWeb do
    pipe_through :browser
    pipe_through :protected

    get "/users", UserController, :index
    delete "/sessions", SessionController, :delete
  end

  # Other scopes may use custom stacks.
  # scope "/api", TwoFaDemoWeb do
  #   pipe_through :api
  # end
end
