defmodule TwoFaDemoWeb.Plugs.EnsureAuthenticated do
  @moduledoc """
  Verifies that the user's organization's subscription is active.
  """
  require Logger
  use Phoenix.Controller
  alias TwoFaDemo.Accounts
  alias TwoFaDemoWeb.Router.Helpers, as: Routes

  def init(opts \\ %{}), do: opts

  def call(conn, _opts) do
    case Plug.Conn.get_session(conn, :session_id) do
      nil ->
        conn
        |> put_flash(:info, "Please log in first.")
        |> redirect(to: Routes.session_path(conn, :new))
        |> halt()
      session_id ->
        case Accounts.get_session_by(session_id: session_id) do
          nil ->
            conn
            |> put_flash(:info, "Please log in first.")
            |> redirect(to: Routes.session_path(conn, :new))
            |> halt()
          _session ->
            conn
        end
    end
  end
end
