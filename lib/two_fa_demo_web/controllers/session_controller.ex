defmodule TwoFaDemoWeb.SessionController do
  use TwoFaDemoWeb, :controller

  alias TwoFaDemo.Accounts
  alias TwoFaDemo.Accounts.User
  alias TwoFaDemo.Accounts.Session
  alias TwoFaDemo.Accounts.Auth


  # Render the login page.
  def new(conn, _params) do
    changeset = Accounts.change_session(%Session{})
    render(conn, "new.html", changeset: changeset)
  end


  # Authenticate the user with the provided credentials, and log them in if successful.
  def create(conn, %{"email" => _email, "password" => _password} = params) do
    case Auth.authenticate(params) do
      {:error, message} ->
        conn
        |> put_flash(:error, message)
        |> render("new.html")
      {:ok, %User{} = user} ->
        case Accounts.create_session(user) do
          {:ok, %{session: session}} ->
            conn
            |> put_flash(:info, "Logged in successfully.")
            |> put_session(:session_id, session.session_id)
            |> redirect(to: Routes.user_path(conn, :index))
          {:error, %Ecto.Changeset{} = changeset} ->
            render(conn, "new.html", changeset: changeset)
        end
    end
  end


  # Log the user out.
  def delete(conn, _params) do
    session = Accounts.get_session_by(session_id: Plug.Conn.get_session(conn, :session_id))
    {:ok, _session} = Accounts.delete_session(session)

    conn
    |> put_flash(:info, "Logged out successfully.")
    |> redirect(to: Routes.session_path(conn, :new))
  end
end
