defmodule TwoFaDemo.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias TwoFaDemo.Repo

  alias TwoFaDemo.Accounts.User


  # USERS

  def list_users do
    Repo.all(User)
  end


  def get_user!(id), do: Repo.get!(User, id)
  def get_user_by(opts), do: Repo.get_by(User, opts)

  def user_locked?(user) do
    user.locked_at != nil
  end


  # Increement the user's login attempts. Used to prevent brute-force attacks.
  def increment_login_attempts(user) do
    new_attempts = user.login_attempts + 1
    if new_attempts == 5 do
      lock_user(user)
    else
      update_user_login_attempts(user, new_attempts)
    end
  end

  def update_user_login_attempts(user, attempt_count) do
    user
    |> User.lock_unlock_changeset(%{login_attempts: attempt_count})
    |> Repo.update()
  end

  def reset_login_attempts(user) do
    attrs = %{login_attempts: 0}
    update_user(user, attrs)
  end

  def lock_user(user) do
    changeset = User.lock_unlock_changeset(user, %{login_attempts: 5, locked_at: Timex.now()})
    Repo.update!(changeset)
  end

  def create_user(attrs \\ %{}) do
    %User{}
    |> User.create_user_changeset(attrs)
    |> Repo.insert()
  end


  def update_user(%User{} = user, attrs) do
    user
    |> User.update_user_changeset(attrs)
    |> Repo.update()
  end


  def delete_user(%User{} = user) do
    Repo.delete(user)
  end


  def change_user(%User{} = user) do
    User.update_user_changeset(user, %{})
  end


  # SESSIONS

  alias TwoFaDemo.Accounts.Session


  def get_session!(id), do: Repo.get!(Session, id)

  def get_session_by(opts) do
    Repo.get_by(Session, opts)
  end


  # Create a session for the given user_id.
  def create_session(user) do
    attrs = %{user_id: user.id}

    Ecto.Multi.new()
    |> Ecto.Multi.run(:reset_login_attempts, fn(_repo, _result) ->
      # Reset the user's login attempts on a successful login
      update_user_login_attempts(user, 0)
    end)
    |> Ecto.Multi.insert(:session, Session.changeset(%Session{}, attrs))
    |> Repo.transaction()
  end


  # Delete the session, i.e. log the user out of their current device.
  def delete_session(%Session{} = session) do
    Repo.delete(session)
  end


  def change_session(%Session{} = session) do
    Session.changeset(session, %{})
  end
end
