defmodule TwoFaDemo.Repo do
  use Ecto.Repo,
    otp_app: :two_fa_demo,
    adapter: Ecto.Adapters.Postgres
end
