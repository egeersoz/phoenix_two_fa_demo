defmodule TwoFaDemo.Accounts.Session do
  use Ecto.Schema
  alias Ecto.UUID
  import Ecto.Changeset

  schema "accounts_sessions" do
    field :session_id, :string
    field :user_id, :id

    timestamps()
  end

  @doc false
  def changeset(session, attrs) do
    session
    |> cast(attrs, [:user_id])
    |> validate_required([:user_id])
    |> generate_session_id()
  end

  defp generate_session_id(current_changeset) do
    current_changeset
    |> put_change(:session_id, UUID.generate())
  end
end
