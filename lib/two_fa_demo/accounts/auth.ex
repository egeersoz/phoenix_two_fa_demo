defmodule TwoFaDemo.Accounts.Auth do

  alias TwoFaDemo.Accounts
  alias TwoFaDemo.Accounts.User

  # Checks if the user exists, and authenticates them using the given password.
  @spec authenticate(map) :: {:error, <<_::160, _::_*376>>} | {:ok, any}
  def authenticate(%{"email" => email, "password" => password}) do
    case Accounts.get_user_by(email: String.downcase(email)) do
      nil ->
        # This means a user with that email does not exist.
        # We return an ambigious 'invalid credentials' error because we may not want to disclose
        # information about whether that email has been registered or not.
        {:error, "Invalid credentials."}
      %User{} = user ->
        # This means the user with the given email has been found, so we continue performing additional checks.
        cond do
          Accounts.user_locked?(user) ->
            # User accounts get locked after too many log in attempts, so we prevent the login.
            {:error, "Your account has been locked due to too many failed login attempts."}
          true ->
            case check_password(user, password) do
              {:ok, user} ->
                # If the password is correct, return a tuple containing the user struct.
                {:ok, user}
              _ ->
                # Any other result from `check_password` leads to a tuple containing our
                # intentionally ambigious 'invalid credentials' error message. We also
                # increment failed login attempts to prevent brute-forcing.
                Accounts.increment_login_attempts(user)
                {:error, "Invalid credentials."}
            end
        end
    end

  end

  def check_password(user, password) do
    Argon2.check_pass(user, password)
  end

end
