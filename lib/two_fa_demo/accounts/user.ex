defmodule TwoFaDemo.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "accounts_users" do
    field :email,             :string
    field :login_attempts,    :integer, null: false, default: 0
    field :locked_at,         :utc_datetime_usec
    field :password_hash,     :string
    field :password,          :string, virtual: true

    timestamps()
  end

  @doc false
  def create_user_changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :password])
    |> validate_required([:email, :password])
    |> put_password_hash()
  end

  def update_user_changeset(user, attrs) do
    user
    |> cast(attrs, [:email])
    |> validate_required([:email])
  end

  def lock_unlock_changeset(user, attrs) do
    user
    |> cast(attrs, [:login_attempts, :locked_at])
    |> validate_required([:login_attempts])
  end

  defp put_password_hash(%Ecto.Changeset{valid?: true, changes: %{password: password}} = changeset) do
    change(changeset, Argon2.add_hash(password))
  end

  defp put_password_hash(changeset), do: changeset
end
