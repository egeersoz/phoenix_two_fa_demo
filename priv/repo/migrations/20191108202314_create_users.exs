defmodule TwoFaDemo.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:accounts_users) do
      add :email, :text
      add :password_hash, :text
      add :locked_at, :utc_datetime_usec
      add :login_attempts, :integer, null: false

      timestamps()
    end

    create unique_index(:accounts_users, [:email], name: :unique_user_emails)

  end
end
