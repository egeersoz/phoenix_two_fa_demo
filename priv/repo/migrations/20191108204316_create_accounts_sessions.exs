defmodule TwoFaDemo.Repo.Migrations.CreateAccountsSessions do
  use Ecto.Migration

  def change do
    create table(:accounts_sessions) do
      add :session_id, :string
      add :user_id, references(:accounts_users, on_delete: :delete_all)

      timestamps()
    end

    create index(:accounts_sessions, [:session_id])
    create index(:accounts_sessions, [:user_id])
  end
end
